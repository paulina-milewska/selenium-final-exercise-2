from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, 'content_title').text == title

    def click_add_project(self):
        new_project = self.browser.find_element(By.CSS_SELECTOR, '#content > article > nav > ul > li:nth-child(1) > a')
        new_project.click()

    def verify_project_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def wait_until_project_is_added(self):
        wait = WebDriverWait(self.browser, 3)
        text_area = (By.CSS_SELECTOR, '#j_info_box > p')
        wait.until(expected_conditions.element_to_be_clickable(text_area))

    def verify_added_project(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '#j_info_box > p').text == title

    def click_projects_tab(self):
        self.browser.find_element(By.CSS_SELECTOR, '#wrapper > ul > li.item2 > a').click()

    def check_if_project_is_added(self):
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_if_project_is_listed(self, project_title):
        assert self.browser.find_element(By.CSS_SELECTOR, '#content > article > table > tbody > tr > td:nth-child(1) > a').text == project_title

    # def click_search_button(self):
    # self.browser.find_element(By.ID, 'icon_search').click()
