

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaMessagesPage:

    def __init__(self, browser):
        self.browser = browser

    # def wait_for_text_arena_load(self):
    # wait = WebDriverWait(self.browser, 10)
    # text_area = (By.CSS_SELECTOR, '#j_msgContent')
    # wait.until(expected_conditions.element_to_be_clickable(text_area))

    def verify_new_project_page_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def insert_random_name(self, random_title):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_title)

    def insert_random_prefix(self, random_prefix):
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_prefix)

    def insert_random_description(self, random_description):
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_description)

    def click_save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()


