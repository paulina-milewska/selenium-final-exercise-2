import pytest
import lorem as lorem

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_message_page import ArenaMessagesPage
from pages.arena.arena_project_page import ArenaProjectPage
from pages.arena.arena_home_page import ArenaHomePage
from pages.utils.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = Chrome()
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_should_display_email_in_user_section(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.verify_displayed_email('administrator@testarena.pl')


def test_should_successfully_login(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_login()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_should_open_messages_and_display_text_area(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    wait = WebDriverWait(browser, 10)
    text_area = (By.CSS_SELECTOR, '#j_msgContent')
    wait.until(expected_conditions.element_to_be_clickable(text_area))


def test_should_open_projects_page(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_should_add_new_project(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()

    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.click_add_project()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.verify_project_title('Dodaj projekt')

    # def test_should_add_new_message(browser):
    random_title = generate_random_text(10)
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.insert_random_name(random_title)

    random_prefix = generate_random_text(4)
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.insert_random_prefix(random_prefix)

    random_description = lorem.sentence()
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.insert_random_description(random_description)

    arena_messages_page.click_save_project()

    arena_project_page.wait_until_project_is_added()
    arena_project_page.verify_added_project('Projekt został dodany')

    arena_project_page.click_projects_tab()
    arena_project_page.verify_title('Projekty')

    browser.find_element(By.CSS_SELECTOR, 'input#search').send_keys(random_title)

    arena_project_page.check_if_project_is_added()

    arena_project_page.verify_if_project_is_listed(random_title)

    browser.quit()

# def test_should_save_new_project(browser):
# arena_message_page = ArenaMessagesPage(browser)


# def test_check_if_new_project_is_added(browser):
# arena_project_page = ArenaProjectPage(browser)

# assert browser.current_url == 'http://demo.testarena.pl/administration/project_view/5602'

# def test_should_find_a_new_project(browser):
# arena_project_page = ArenaProjectPage(browser)


# browser.find_element(By.ID, 'name').send_keys('random_text')
# browser.find_element(By.ID, 'prefix').send_keys('random_prefix')
